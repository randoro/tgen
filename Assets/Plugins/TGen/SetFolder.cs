using System.Collections;
using System.Collections.Generic;
using NodeEditorFramework.Standard;
using TGen.Standard;
using UnityEngine;
using WorleyNoise;

namespace TGen.Standard
{
    public static class SetFolder
    {


        public static float[,] GenerateRootHeights(Vector2 size, Vector2 center, bool useRandomSeed, Set root)
        {
            return GenerateHeights(size, center, useRandomSeed, root);
        }

        private static float[,] GenerateHeights(Vector2 size, Vector2 center, bool useRandomSeed, Set set)
        {
            if (set.GetType() == typeof (PerlinSet))
            {
                return GeneratePerlinHeights(size, center, useRandomSeed, (PerlinSet) set);
            }
            else if (set.GetType() == typeof(WorleySet))
            {
                return GenerateWorleyHeights(size, useRandomSeed, (WorleySet)set);
            }
            else if (set.GetType() == typeof(MergeSet))
            {
                return UnfoldMergeHeights(size, center, useRandomSeed, (MergeSet)set);
            }

            return null;
        }

        private static float[,] GeneratePerlinHeights(Vector2 size, Vector2 center, bool useRandomSeed, PerlinSet set)
        {
            if (useRandomSeed)
            {
                System.Random rand = new System.Random();
                return PerlinGenerator.GenerateNoiseMap((int)size.x, (int)size.y, rand.Next(0, 1000000), set.Scale, set.Octaves,
                set.Persistance, set.Lacunarity, center);
            }
            return PerlinGenerator.GenerateNoiseMap((int) size.x, (int) size.y, set.Seed, set.Scale, set.Octaves,
                set.Persistance, set.Lacunarity, center);

        }

        private static float[,] GenerateWorleyHeights(Vector2 size, bool useRandomSeed, WorleySet set)
        {
            if (useRandomSeed)
            {
                System.Random rand = new System.Random();
                return Worley.GenerateImage((int)size.x, (int)size.y, 0, set.Scale, set.PointsPerSquare, set.DistanceFunction, set.ValueFunction, rand.Next(0, 1000000));
            }
            return Worley.GenerateImage((int)size.x, (int)size.y, 0, set.Scale, set.PointsPerSquare, set.DistanceFunction, set.ValueFunction, set.Seed);

        }

        private static float[,] UnfoldMergeHeights(Vector2 size, Vector2 center, bool useRandomSeed, MergeSet set)
        {
            float[,] firstset = GenerateHeights(size, center, useRandomSeed, set.Set1);
            float[,] secondset = GenerateHeights(size, center, useRandomSeed, set.Set2);
            float[,] endset = new float[(int)size.x, (int)size.y];

            switch (set.MergeFunction)
            {
                   case MergeFunction.Add:

                    for (int y = 0; y < size.y; y++)
                    {
                        for (int x = 0; x < size.x; x++)
                        {
                            endset[x, y] = firstset[x, y]*set.QuantitySet1 + secondset[x, y]*set.QuantitySet2;
                        }
                    }

                    break;

                case MergeFunction.Multiply:

                    for (int y = 0; y < size.y; y++)
                    {
                        for (int x = 0; x < size.x; x++)
                        {
                            endset[x, y] = firstset[x, y] * set.QuantitySet1 * secondset[x, y] * set.QuantitySet2;
                        }
                    }

                    break;
                default:
                    break;

            }
            return endset;

        }

    }


}
