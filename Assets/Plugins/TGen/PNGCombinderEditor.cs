using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PNGCombinder))]
public class PNGCombinderEditor : Editor
{

    private PNGCombinder data = null;

    public void OnEnable()
    {
        data = (PNGCombinder)target;
    }

    public override void OnInspectorGUI()
    {

        if (DrawDefaultInspector())
        {
            
        }

        if (GUILayout.Button("Generate", GUILayout.Height(40)))
        {
            data.CombinePNGs();
            //EditorUtility.SetDirty(target);
        }
    }
}
