using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using NodeEditorFramework;
using NodeEditorFramework.Standard;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TGen.Standard
{
    [ExecuteInEditMode]
    public class TerrainGenerator : MonoBehaviour
    {
        
        static Queue<MapThreadInfo<MapData>> mapDataThreadInfoQueue = new Queue<MapThreadInfo<MapData>>();
        static Queue<MapThreadInfo<SplatData>> splatDataThreadInfoQueue = new Queue<MapThreadInfo<SplatData>>();
        
        #region Threading

        public static void RequestMapData(Set set, Vector2 center, Vector2 size, int GenerationID, bool useRandomSeed, Action<MapData> callback)
        {
            //mapDataThreadInfoQueue.Clear(); seems to work without
            ThreadStart threadStart = delegate
            {
                MapDataThread(set, center, size, GenerationID, useRandomSeed, callback);
            };

            new Thread(threadStart).Start();
        }

        static void MapDataThread(Set set, Vector2 center, Vector2 size, int GenerationID, bool useRandomSeed, Action<MapData> callback)
        {
            MapData mapData = GenerateMapData(size, center, GenerationID, useRandomSeed, set);
            lock (mapDataThreadInfoQueue)
            {
                mapDataThreadInfoQueue.Enqueue(new MapThreadInfo<MapData>(callback, mapData));
            }
        }

        public static void RequestSplatData(SplatSet set, int GenerationID, Action<SplatData> callback)
        {
            //mapDataThreadInfoQueue.Clear(); seems to work without
            ThreadStart threadStart = delegate
            {
                SplatDataThread(set, GenerationID, callback);
            };

            new Thread(threadStart).Start();
        }

        static void SplatDataThread(SplatSet set, int GenerationID, Action<SplatData> callback)
        {
            SplatData splatData = GenerateSplatData(GenerationID, set);
            lock (splatDataThreadInfoQueue)
            {
                splatDataThreadInfoQueue.Enqueue(new MapThreadInfo<SplatData>(callback, splatData));
            }
        }

        struct MapThreadInfo<T>
        {
            public readonly Action<T> callback;
            public readonly T parameter;

            public MapThreadInfo(Action<T> callback, T parameter)
            {
                this.callback = callback;
                this.parameter = parameter;
            }
        }

        #endregion


        #region Mono
        

        void OnRenderObject()
        {
            if (mapDataThreadInfoQueue.Count > 0)
            {
                ProcessGeneration();
            }

            if (splatDataThreadInfoQueue.Count > 0)
            {
                ProcessSplat();
            }
        }

        public static void ProcessGeneration()
        {
            MapThreadInfo<MapData> threadInfo = mapDataThreadInfoQueue.Dequeue();
            threadInfo.callback(threadInfo.parameter);
        }

        public static void ProcessSplat()
        {
            MapThreadInfo<SplatData> threadInfo = splatDataThreadInfoQueue.Dequeue();
            threadInfo.callback(threadInfo.parameter);
        }

        #endregion

        #region Generator

        static MapData GenerateMapData(Vector2 size, Vector2 center, int GenerationID, bool useRandomSeed, Set set)
        {

            float[,] heights = SetFolder.GenerateRootHeights(size, center, useRandomSeed, set);
            
            return new MapData(heights, GenerationID);
        }

        static SplatData GenerateSplatData(int GenerationID, SplatSet set)
        {
            float[,] heights = set.heights;
            int SizeMinusOne = heights.GetLength(0) - 1;
            float[,,] splat = new float[SizeMinusOne, SizeMinusOne, 5];

            for (int y = 0; y < SizeMinusOne; y++)
            {
                for (int x = 0; x < SizeMinusOne; x++)
                {
                    splat[x, y, 0] = 0.01f;
                }
            }


            if (set.T1)
            {
                for (int y = 0; y < SizeMinusOne; y++)
                {
                    for (int x = 0; x < SizeMinusOne; x++)
                    {
                        int intHeight = (int)(heights[x, y] * 100);
                        int intClampHeight = Mathf.Clamp(intHeight, 0, 99);
                        splat[x, y, 1] = Mathf.Clamp01(set.curveValue1[intClampHeight]) + 0.001f;
                    }
                }
            }

            if (set.T2)
            {
                for (int y = 0; y < SizeMinusOne; y++)
                {
                    for (int x = 0; x < SizeMinusOne; x++)
                    {
                        int intHeight = (int)(heights[x, y] * 100);
                        int intClampHeight = Mathf.Clamp(intHeight, 0, 99);
                        splat[x, y, 2] = Mathf.Clamp01(set.curveValue2[intClampHeight]) + 0.001f;
                    }
                }
            }

            if (set.T3)
            {
                for (int y = 0; y < SizeMinusOne; y++)
                {
                    for (int x = 0; x < SizeMinusOne; x++)
                    {
                        int intHeight = (int)(heights[x, y] * 100);
                        int intClampHeight = Mathf.Clamp(intHeight, 0, 99);
                        splat[x, y, 3] = Mathf.Clamp01(set.curveValue3[intClampHeight]) + 0.001f;
                    }
                }
            }

            if (set.T4)
            {
                for (int y = 0; y < SizeMinusOne; y++)
                {
                    for (int x = 0; x < SizeMinusOne; x++)
                    {
                        int intHeight = (int)(heights[x, y] * 100);
                        int intClampHeight = Mathf.Clamp(intHeight, 0, 99);
                        splat[x, y, 4] = Mathf.Clamp01(set.curveValue4[intClampHeight]) + 0.001f;
                    }
                }
            }

            return new SplatData(splat, GenerationID);
        }

        #endregion
    }

    public struct MapData
    {
        public readonly float[,] heightMap;
        public readonly int GenerationID;

        public MapData(float[,] heightMap, int GenerationID)
        {
            this.heightMap = heightMap;
            this.GenerationID = GenerationID;

        }
    }

    public struct SplatData
    {
        public readonly float[,,] splatMap;
        public readonly int SplatGenerationID;

        public SplatData(float[,,] splatMap, int SplatGenerationID)
        {
            this.splatMap = splatMap;
            this.SplatGenerationID = SplatGenerationID;

        }
    }
}
