using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

public class PNGCombinder : MonoBehaviour
{

    public Texture2D[] inputPictures;


    public void CombinePNGs()
    {
        if (inputPictures == null || inputPictures.Length == 0)
        {
            Debug.Log("inputPictures is null or empty.");
            return;
        }

        for (int i = 0; i < inputPictures.Length; i++)
        {
            if(inputPictures[i] == null || inputPictures[i].width != 1000 || inputPictures[i].height != 1000)
            {

                Debug.Log("one or more pictures (nr " +i+" ) in inputPictures are null. or wrong size");
                return;
            }

        }


        //Create Texture

        Texture2D combination = new Texture2D(1000, 1000);
        float[] combinationHolder = new float[1000*1000];

        string path = EditorUtility.SaveFilePanelInProject("Combined Range", "Combined", "asset", "", "Assets/Plugins/TGen/Resources/Expressive/");

        //Combine

        for (int i = 0; i < inputPictures.Length; i++)
        {

           Color[] raw = inputPictures[i].GetPixels();

            for (int j = 0; j < raw.Length; j++)
            {
                combinationHolder[j] += raw[j].r;
            }
        }

        //Divide with amount? to ease picture





        //Add to texture
        Color[] combinedColors = new Color[1000 * 1000];
        for (int i = 0; i < combinedColors.Length; i++)
        {
            combinedColors[i].a = 1;
            combinedColors[i].r = combinationHolder[i];
            combinedColors[i].g = combinationHolder[i];
            combinedColors[i].b = combinationHolder[i];
        }

        combination.SetPixels(combinedColors);


        //Fill picture
        if (!string.IsNullOrEmpty(path))
        {
            UnityEditor.AssetDatabase.CreateAsset(combination, path);
            
            byte[] bytes = combination.EncodeToPNG();
            File.WriteAllBytes(path + ".Picture.png", bytes);

            path = null;
        }
    }
}
