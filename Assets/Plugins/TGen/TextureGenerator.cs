﻿using System.Collections;
using System.Collections.Generic;
using NodeEditorFramework.Standard;
using UnityEngine;
using WorleyNoise;

namespace TGen.Standard
{
    public static class TextureGenerator
    {

        public static Texture2D TextureFromColorMap(Color[] colorMap, int width, int height)
        {
            Texture2D texture = new Texture2D(width, height);
            texture.filterMode = FilterMode.Point;
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.SetPixels(colorMap);
            texture.Apply();
            return texture;
        }

        public static Texture2D TextureFromFloatMap(float[,] heightMap)
        {
            int width = heightMap.GetLength(0);
            int height = heightMap.GetLength(1);

            Color[] colorMap = new Color[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, heightMap[x, y]);
                }
            }

            return TextureFromColorMap(colorMap, width, height);
        }

        public static Texture2D TextureFromByteMap(byte[,] byteMap)
        {
            int width = byteMap.GetLength(0);
            int height = byteMap.GetLength(1);

            Color[] colorMap = new Color[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, byteMap[x, y] / 255.0f);
                }
            }

            return TextureFromColorMap(colorMap, width, height);
        }

        public static Texture2D TextureFromPerlinSet(int width, int height, PerlinSet set)
        {
            float[,] valueMap = PerlinGenerator.GenerateNoiseMap(width, height, set.Seed, set.Scale, set.Octaves,
                set.Persistance, set.Lacunarity, new Vector2(set.OffsetX, set.OffsetY));

            return TextureFromFloatMap(valueMap);
        }

        public static Texture2D TextureFromWorleySet(int width, int height, WorleySet set)
        {
            float[,] valueMap = Worley.GenerateImage(width, height, 0, set.Scale, set.PointsPerSquare, set.DistanceFunction, set.ValueFunction, set.Seed);

            return TextureFromFloatMap(valueMap);
        }



        public static Texture2D ExpressiveRangeTextureFromIntMap(int[,] intMap)
        {
            int width = intMap.GetLength(0);
            int height = intMap.GetLength(1);

            Color[] colorMap = new Color[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, intMap[x, y] / 10.0f);
                }
            }

            return TextureFromColorMap(colorMap, width, height);
        }





    }
}
