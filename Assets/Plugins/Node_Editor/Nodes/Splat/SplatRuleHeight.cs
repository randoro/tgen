using System;
using UnityEngine;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using UnityEditor;

namespace NodeEditorFramework.Standard
{
	[Node (false, "Splat/Splat Rule Height")]
	public class SplatRuleHeight : Node 
	{
		public const string ID = "splatRuleHeightNode";
		public override string GetID { get { return ID; } }

        [SerializeField]
	    public AnimationCurve HeightCurve;

        
        public override Node Create (Vector2 pos) 
		{
            SplatRuleHeight node = CreateInstance<SplatRuleHeight> ();
			
			node.rect = new Rect (pos.x, pos.y, 280, 180);
			node.name = "Splat Rule Height";

            HeightCurve = new AnimationCurve();

            node.CreateOutput ("Output", "SplatRuleHeightSet");

            //EditorGUILayout.CurveField(new AnimationCurve(), GUILayout.Width(100));
            return node;
		}
		
		protected internal override void NodeGUI () 
		{
            //GUILayout.BeginHorizontal();

            ////Inputs [0].DisplayLayout ();

            //GUILayout.EndVertical ();
            //         GUILayout.BeginVertical ();

            ////Outputs [0].DisplayLayout ();
            
            GUILayout.Label("Rule Settings");
            RTEditorGUI.Seperator();


            
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Curve", "Defines the texture intensity on specific height values."), GUILayout.Width(100));
		    if (HeightCurve != null)
		    {
		        EditorGUILayout.CurveField(HeightCurve, GUILayout.Width(150));
		    }
		    else
		    {
                HeightCurve = new AnimationCurve();
            }
		    GUILayout.EndHorizontal();
            
            

            if (GUI.changed)
		        NodeEditor.RecalculateFrom(this);
		}
		
		public override bool Calculate () 
		{
            //if (!allInputsReady ()) //maybe need change?
            //	return false;

      //      float[] curveValues = new float[100];
		    //if (HeightCurve != null)
		    //{
		    //    for (int i = 0; i < 100; i++)
		    //    {
		    //        curveValues[i] = HeightCurve.Evaluate(0.01f*i);
		    //    }
		    //}


		    Outputs[0].SetValue<SplatRuleHeightSet> (new SplatRuleHeightSet(HeightCurve));
            
            return true;
		}
	}

}