using System;
using UnityEngine;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using UnityEditor;

namespace NodeEditorFramework.Standard
{
	[Node (false, "Splat/Splat Manager")]
	public class SplatManager : Node 
	{
		public const string ID = "splatManagerNode";
		public override string GetID { get { return ID; } }

        [SerializeField]
        public string TexturePath1;
        [SerializeField]
        public string TexturePath2;
        [SerializeField]
        public string TexturePath3;
        [SerializeField]
        public string TexturePath4;

        private Texture2D Texture1;
	    private Texture2D Texture1_norm;

        private Texture2D Texture2;
        private Texture2D Texture2_norm;

        private Texture2D Texture3;
        private Texture2D Texture3_norm;

        private Texture2D Texture4;
        private Texture2D Texture4_norm;

        public override Node Create (Vector2 pos) 
		{
            SplatManager node = CreateInstance<SplatManager> ();
			
			node.rect = new Rect (pos.x, pos.y, 380, 780);
			node.name = "Splat Manager";

            TexturePath1 = "";
            TexturePath2 = "";
            TexturePath3 = "";
            TexturePath4 = "";

            node.CreateInput("SplatRule1", "SplatRuleHeightSet");
            node.CreateInput("SplatRule2", "SplatRuleHeightSet");
            node.CreateInput("SplatRule3", "SplatRuleHeightSet");
            node.CreateInput("SplatRule4", "SplatRuleHeightSet");
            node.CreateOutput ("Output", "SplatSet");

            //EditorGUILayout.CurveField(new AnimationCurve(), GUILayout.Width(100));
            return node;
		}
		
		protected internal override void NodeGUI () 
		{
            //GUILayout.BeginHorizontal();

            ////Inputs [0].DisplayLayout ();

            //GUILayout.EndVertical ();
            //         GUILayout.BeginVertical ();

            ////Outputs [0].DisplayLayout ();
            
            GUILayout.Label("Terrain Settings");
            RTEditorGUI.Seperator();



            GUILayout.Label("Texture 1");
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Path", "The path to the texture file in Resource folder."), GUILayout.Width(100));
            TexturePath1 = RTEditorGUI.TextField(GUIContent.none, TexturePath1, GUIStyle.none, GUILayout.Width(100));
            GUILayout.EndHorizontal();
            if (TexturePath1 != null && TexturePath1.Length > 0)
		    {
                Texture1 = Resources.Load(TexturePath1, typeof(Texture2D)) as Texture2D;
		        if (Texture1)
		        {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Texture Preview", "Preview of the texture."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture1, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
                Texture1_norm = Resources.Load(TexturePath1+"_norm", typeof(Texture2D)) as Texture2D;
                if (Texture1_norm)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Normal Preview", "Preview of the normal map."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture1_norm, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
            }
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("T1 Splatrule", "Splat Rule used to splat the terrain."), GUILayout.Width(100));
            InputKnob(0);
            GUILayout.EndHorizontal();
            RTEditorGUI.Seperator();



            GUILayout.Label("Texture 2");
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Path", "The path to the texture file in Resource folder."), GUILayout.Width(100));
            TexturePath2 = RTEditorGUI.TextField(GUIContent.none, TexturePath2, GUIStyle.none, GUILayout.Width(100));
            GUILayout.EndHorizontal();
            if (TexturePath2 != null && TexturePath2.Length > 0)
            {
                Texture2 = Resources.Load(TexturePath2, typeof(Texture2D)) as Texture2D;
                if (Texture2)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Texture Preview", "Preview of the texture."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture2, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
                Texture2_norm = Resources.Load(TexturePath2 + "_norm", typeof(Texture2D)) as Texture2D;
                if (Texture2_norm)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Normal Preview", "Preview of the normal map."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture2_norm, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
            }
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("T2 Splatrule", "Splat Rule used to splat the terrain."), GUILayout.Width(100));
            InputKnob(1);
            GUILayout.EndHorizontal();
            RTEditorGUI.Seperator();




            GUILayout.Label("Texture 3");
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Path", "The path to the texture file in Resource folder."), GUILayout.Width(100));
            TexturePath3 = RTEditorGUI.TextField(GUIContent.none, TexturePath3, GUIStyle.none, GUILayout.Width(100));
            GUILayout.EndHorizontal();
            if (TexturePath3 != null && TexturePath3.Length > 0)
            {
                Texture3 = Resources.Load(TexturePath3, typeof(Texture2D)) as Texture2D;
                if (Texture3)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Texture Preview", "Preview of the texture."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture3, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
                Texture3_norm = Resources.Load(TexturePath3 + "_norm", typeof(Texture2D)) as Texture2D;
                if (Texture3_norm)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Normal Preview", "Preview of the normal map."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture3_norm, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
            }
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("T3 Splatrule", "Splat Rule used to splat the terrain."), GUILayout.Width(100));
            InputKnob(2);
            GUILayout.EndHorizontal();
            RTEditorGUI.Seperator();



            GUILayout.Label("Texture 4");
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Path", "The path to the texture file in Resource folder."), GUILayout.Width(100));
            TexturePath4 = RTEditorGUI.TextField(GUIContent.none, TexturePath4, GUIStyle.none, GUILayout.Width(100));
            GUILayout.EndHorizontal();
            if (TexturePath4 != null && TexturePath4.Length > 0)
            {
                Texture4 = Resources.Load(TexturePath4, typeof(Texture2D)) as Texture2D;
                if (Texture4)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Texture Preview", "Preview of the texture."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture4, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
                Texture4_norm = Resources.Load(TexturePath4 + "_norm", typeof(Texture2D)) as Texture2D;
                if (Texture4_norm)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Normal Preview", "Preview of the normal map."), GUILayout.Width(100));
                    RTEditorGUI.DrawTexture(Texture4_norm, 50, GUIStyle.none);
                    GUILayout.EndHorizontal();
                }
            }
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("T4 Splatrule", "Splat Rule used to splat the terrain."), GUILayout.Width(100));
            InputKnob(3);
            GUILayout.EndHorizontal();
            RTEditorGUI.Seperator();





            if (GUI.changed)
		        NodeEditor.RecalculateFrom(this);
		}
		
		public override bool Calculate () 
		{
            //if (!allInputsReady ()) //maybe need change?
            //	return false;

            AnimationCurve C1 = null;
            AnimationCurve C2 = null;
            AnimationCurve C3 = null;
            AnimationCurve C4 = null;

            if (Inputs[0].connection != null)
            {
                C1 = Inputs[0].GetValue<SplatRuleHeightSet>().C;
            }
            if (Inputs[1].connection != null)
            {
                C2 = Inputs[1].GetValue<SplatRuleHeightSet>().C;
            }
            if (Inputs[2].connection != null)
            {
                C3 = Inputs[2].GetValue<SplatRuleHeightSet>().C;
            }
            if (Inputs[3].connection != null)
            {
                C4 = Inputs[3].GetValue<SplatRuleHeightSet>().C;
            }


            Outputs[0].SetValue<SplatSet> (new SplatSet(Texture1, Texture1_norm, C1, Texture2, Texture2_norm, C2, Texture3, Texture3_norm, C3, Texture4, Texture4_norm, C4));
            
            return true;
		}
	}

}