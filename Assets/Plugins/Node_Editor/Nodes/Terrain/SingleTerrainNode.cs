using System;
using System.IO;
using System.Linq;
using UnityEngine;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using TGen.Standard;
using UnityEditor;

namespace NodeEditorFramework.Standard
{
    [Node(false, "Terrain/Single Terrain")]
    public class SingleTerrainNode : Node
    {
        public const string ID = "singleTerrainNode";
        public override string GetID { get { return ID; } }
        
        [SerializeField]
        public int TerrainInstanceID;
        

        [SerializeField]
        public string TerrainName;
        [SerializeField]
        public int SizeExponent;
        [SerializeField]
        public int Size;

        [SerializeField]
        public bool AutoRefresh;

        [SerializeField]
        public bool ExpressEnabled;

        [SerializeField]
        public int Tests;

        [SerializeField]
        public SplatPrototype[] SplatGroup;


        private GameObject foundGameObject;
        private Terrain foundTerrain;
        private TerrainCollider foundTerrainCollider;
        private TerrainData foundTerrainData;

        private MapData[] ExpressiveMapDatas;
        private int ExpressiveDone;

        private NodeEditorState tempState;
        private int GenerationID;
        private int SplatGenerationID;
        private int ExpressGenerationID;
        private string path;


        public override Node Create(Vector2 pos)
        {
            SingleTerrainNode node = CreateInstance<SingleTerrainNode>();
            
            node.rect = new Rect(pos.x, pos.y, 280, 380);
            node.name = "Single Terrain";

            SizeExponent = 5;
            Tests = 5;

            node.CreateInput("Noise", "Set");
            node.CreateInput("Splat", "SplatSet");
            //node.CreateOutput("Output", "PerlinSet");
            

            return node;
        }
        

        protected internal override void OnDelete()
        {
            //Delete terrain
            foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("Terrain"))
            {
                if (fooObj.GetInstanceID() == TerrainInstanceID)
                {
                    DestroyImmediate(fooObj);
                    break;
                }
            }
            base.OnDelete();
        }

        protected internal override void NodeGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Noise");
            InputKnob(0);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Splat");
            InputKnob(1);
            GUILayout.EndHorizontal();



            GUILayout.Label("Terrain Settings");
            RTEditorGUI.Seperator();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Name", "Defines the name of the terrain."), GUILayout.Width(100));
            TerrainName = RTEditorGUI.TextField(GUIContent.none, TerrainName, GUIStyle.none, GUILayout.Width(150));
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("SizeExponent", "Defines the exponent used to calculate the size of the terrain."), GUILayout.Width(100));
            SizeExponent = RTEditorGUI.IntSlider(GUIContent.none, SizeExponent, 1, 10, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Size = "+Size+"x"+Size, "Defines the actual length of the terrain x- and z-axis."), GUILayout.Width(200));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Auto Refresh", "Defines if the terrain auto generates whenever a variable changes."), GUILayout.Width(200));
            AutoRefresh = RTEditorGUI.Toggle(AutoRefresh, GUIContent.none, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            if (!AutoRefresh)
            {
                if (GUILayout.Button(new GUIContent("Generate Terrain", "Generate the terrain.")))
                {
                    if (Inputs[0].connection != null)
                    {
                        CreateTerrain();
                        GenerateTerrain();
                    }
                }
                if (GUILayout.Button(new GUIContent("Generate Splat", "Generate the splat textures on the terrain.")))
                {
                    if (Inputs[1].connection != null)
                    {
                        GenerateSplat();
                    }
                }
            }
            


            if (GUILayout.Button(new GUIContent("Save TerrainData", "Saves the TerrainData seperatly in the Assets folder.")))
            {
                string path = EditorUtility.SaveFilePanelInProject("Save Terrain Data", "Terrain Data", "asset", "", "Assets/Plugins/TGen/Resources/TerrainData/");
                if (!string.IsNullOrEmpty(path))
                    UnityEditor.AssetDatabase.CreateAsset(foundTerrainData, path);
            }

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Debug (Don't use)", "For debugging."), GUILayout.Width(200));
            ExpressEnabled = RTEditorGUI.Toggle(ExpressEnabled, GUIContent.none, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            if (ExpressEnabled)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(new GUIContent("Tests", "Amount of expressive range tests."), GUILayout.Width(100));
                Tests = RTEditorGUI.IntField(Tests, GUILayout.Width(150));
                GUILayout.EndHorizontal();

                if (GUILayout.Button(new GUIContent("Generate Expressive Range", "Generate expressive range (This takes long time).")))
                {
                    if (Inputs[0].connection != null)
                    {
                        CreateTerrain();
                        GenerateExpressiveRange();
                    }
                }
            }




            if (GUI.changed)
                NodeEditor.RecalculateFrom(this);
        }

        public override bool Calculate()
        {
            //if (!allInputsReady ()) //maybe need change?
            //	return false;

            Size = Mathf.FloorToInt(Mathf.Pow(2, SizeExponent)) + 1;

            Node splatMan = Inputs[1].GetNodeAcrossConnection();
            if (splatMan)
            {
                splatMan.Calculate();
            }

            if (Inputs[0].connection != null)
            {
                //Seed = Inputs[0].GetValue<Set>();

                CreateTerrain();

                if (AutoRefresh)
                {
                    
                    GenerateTerrain();
                }
            }

            if (Inputs[1].connection != null)
            {

                if (AutoRefresh)
                {
                    GenerateSplat();
                }
            }


            return true;
        }



        public void GenerateSplat()
        {
            if (foundGameObject != null)
            {
                if (foundTerrain != null)
                {
                    if (foundTerrain.terrainData != null)
                    {

                        float[,] heights = foundTerrain.terrainData.GetHeights(0, 0, Size, Size);
                        SplatSet splat = Inputs[1].GetValue<SplatSet>();
                        splat.heights = heights;


                        


                        if (SplatGroup == null)
                        {
                            SplatGroup = new SplatPrototype[5];
                        }

                        Texture2D whiteText = Resources.Load("Textures/whiteback", typeof(Texture2D)) as Texture2D;

                        SplatGroup[0] = new SplatPrototype();
                        SplatGroup[0].texture = whiteText;
                        SplatGroup[0].normalMap = whiteText;

                        if (splat.T1)
                        {
                            SplatGroup[1] = new SplatPrototype();
                            SplatGroup[1].texture = splat.T1;
                            SplatGroup[1].normalMap = splat.T1N;

                            float[] curveValues = new float[100];
                            if (splat.T1C != null)
                            {
                                for (int i = 0; i < 100; i++)
                                {
                                    curveValues[i] = splat.T1C.Evaluate(0.01f * i);
                                }

                                splat.curveValue1 = curveValues;
                            }
                        }
                        if (splat.T2)
                        {
                            SplatGroup[2] = new SplatPrototype();
                            SplatGroup[2].texture = splat.T2;
                            SplatGroup[2].normalMap = splat.T2N;

                            float[] curveValues = new float[100];
                            if (splat.T2C != null)
                            {
                                for (int i = 0; i < 100; i++)
                                {
                                    curveValues[i] = splat.T2C.Evaluate(0.01f * i);
                                }

                                splat.curveValue2 = curveValues;
                            }
                        }
                        if (splat.T3)
                        {
                            SplatGroup[3] = new SplatPrototype();
                            SplatGroup[3].texture = splat.T3;
                            SplatGroup[3].normalMap = splat.T3N;

                            float[] curveValues = new float[100];
                            if (splat.T3C != null)
                            {
                                for (int i = 0; i < 100; i++)
                                {
                                    curveValues[i] = splat.T3C.Evaluate(0.01f * i);
                                }

                                splat.curveValue3 = curveValues;
                            }
                        }
                        if (splat.T4)
                        {
                            SplatGroup[4] = new SplatPrototype();
                            SplatGroup[4].texture = splat.T4;
                            SplatGroup[4].normalMap = splat.T4N;

                            float[] curveValues = new float[100];
                            if (splat.T4C != null)
                            {
                                for (int i = 0; i < 100; i++)
                                {
                                    curveValues[i] = splat.T4C.Evaluate(0.01f * i);
                                }

                                splat.curveValue4 = curveValues;
                            }
                        }
                        foundTerrain.terrainData.splatPrototypes = SplatGroup;

                        SplatGenerationID++;
                        TerrainGenerator.RequestSplatData(Inputs[1].GetValue<SplatSet>(), SplatGenerationID, SplatCalculateCallback);

                        if (NodeEditor.curEditorState != null)
                        {
                            tempState = NodeEditor.curEditorState;
                            NodeEditor.curEditorState.generatorStatus = "Generating Splat...";
                        }

                    }
                }
            }
        }


        public void CreateTerrain()
        {
            
            if (foundGameObject == null)
            {
                foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("Terrain"))
                {
                    if (fooObj.GetInstanceID() == TerrainInstanceID)
                    {
                        foundGameObject = fooObj;
                        break;
                    }
                }
            }

            if (foundGameObject != null)
            {
                //set variables
                foundGameObject.name = TerrainName;

            }
            else
            {
                //Instansiate new
                GameObject newObj = Instantiate(Resources.Load("TerrainPrefabs/SingleTerrain") as GameObject);
                TerrainInstanceID = newObj.GetInstanceID();
                TerrainName = newObj.name;
                newObj.transform.parent = NodeEditor.gameObjectHolder.transform;
                
                foundGameObject = newObj;

                InitTerrainData();

            }

            
        }

        public void InitTerrainData()
        {
            if (foundTerrainData == null)
            {
                foundTerrainData = new TerrainData();
                //Settings
            }
            foundTerrainData.heightmapResolution = Size;
            foundTerrainData.baseMapResolution = Size - 1;
            foundTerrainData.SetDetailResolution(Size - 1, 16);
            foundTerrainData.size = new Vector3(Size - 1, 100, Size - 1);
            foundTerrainData.alphamapResolution = Size - 1;

            foundTerrain = foundGameObject.GetComponent<Terrain>();
            foundTerrainCollider = foundGameObject.GetComponent<TerrainCollider>();

            foundTerrainCollider.terrainData = foundTerrainData;
            foundTerrain.terrainData = foundTerrainData;

        }


        public void GenerateTerrain()
        {
            InitTerrainData();

            foundTerrain = foundGameObject.GetComponent<Terrain>();
            foundTerrainCollider = foundGameObject.GetComponent<TerrainCollider>();

            foundGameObject.SetActive(false);

            GenerationID++;
            TerrainGenerator.RequestMapData(Inputs[0].GetValue<Set>(), new Vector2(0, 0), new Vector2(Size, Size), GenerationID, false, CalculateCallback);

            if (NodeEditor.curEditorState != null)
            {
                tempState = NodeEditor.curEditorState;
                NodeEditor.curEditorState.generatorStatus = "Generating...";
            }
        }


        public void CalculateCallback(MapData mapData)
        {
            if (foundTerrain != null && foundGameObject != null && foundTerrain.terrainData != null)
            {
                if (mapData.GenerationID == GenerationID)
                {
                    foundGameObject.SetActive(true);
                    foundTerrain.terrainData.SetHeights(0, 0, mapData.heightMap);
                    foundTerrain.Flush();

                    GenerateSplat();
                    if (tempState)
                    {
                        tempState.generatorStatus = "Generation Done!";
                    }
                }
            }
            
        }


        public void SplatCalculateCallback(SplatData SplatData)
        {
            if (foundTerrain != null && foundGameObject != null && foundTerrain.terrainData != null)
            {
                if (SplatData.SplatGenerationID == SplatGenerationID)
                {
                    foundTerrain.terrainData.SetAlphamaps(0, 0, SplatData.splatMap);
                    //foundTerrain.terrainData.SetHeights(0, 0, mapData.heightMap);
                    foundTerrain.Flush();
                    if (tempState)
                    {
                        tempState.generatorStatus = "Splat Done!";
                    }
                }
            }

        }


        public void GenerateExpressiveRange()
        {
            InitTerrainData();

            foundTerrain = foundGameObject.GetComponent<Terrain>();
            foundTerrainCollider = foundGameObject.GetComponent<TerrainCollider>();

            foundGameObject.SetActive(false);

            path = EditorUtility.SaveFilePanelInProject("Expressive Range", "ER Data", "asset", "", "Assets/Plugins/TGen/Resources/Expressive/");

            ExpressGenerationID = 0;
            ExpressiveDone = 0;
            //int amount = 5;
            ExpressiveMapDatas = new MapData[Tests];

           // for (int i = 0; i < Tests; i++)
            //{
                ExpressGenerationID++;
                TerrainGenerator.RequestMapData(Inputs[0].GetValue<Set>(), new Vector2(0, 0), new Vector2(Size, Size), ExpressGenerationID, true, CalculateExpressCallback);
            //}

            if (NodeEditor.curEditorState != null)
            {
                tempState = NodeEditor.curEditorState;
                NodeEditor.curEditorState.generatorStatus = "Generating "+ Tests + " Expressive...";
            }
        }


        public void CalculateExpressCallback(MapData mapData)
        {
            if (foundTerrain != null && foundGameObject != null && foundTerrain.terrainData != null)
            {
                Debug.Log("CalcExp for nr:"+ mapData.GenerationID);

                ExpressiveMapDatas[mapData.GenerationID-1] = mapData;
                ExpressiveDone++;

                if (tempState)
                {
                    tempState.generatorStatus = "Generation Nr " + ExpressGenerationID + " Done!";
                }

                if (ExpressiveDone >= Tests)
                {
                    Debug.Log("CalcExp for nr:" + mapData.GenerationID + " was last. All are done.");
                    CreateExpressPicture();
                }
                else
                {
                    ExpressGenerationID++;
                    TerrainGenerator.RequestMapData(Inputs[0].GetValue<Set>(), new Vector2(0, 0), new Vector2(Size, Size), ExpressGenerationID, true, CalculateExpressCallback);
                }

                //if (mapData.GenerationID == ExpressGenerationID)
                //{
                //    Debug.Log("CalcExp for nr:" + mapData.GenerationID+" matches, so its all finished.");
                //    foundGameObject.SetActive(true);
                //    foundTerrain.terrainData.SetHeights(0, 0, mapData.heightMap);
                //    foundTerrain.Flush();

                //    int ab = 1;

                //}

                
            }
            
        }

        private void CreateExpressPicture()
        {
            double[] linearityArray = new double [Tests];

            for (int i = 0; i < Tests; i++)
            {
                MapData current = ExpressiveMapDatas[i];

                double linearity = 0.0f;

                for (int x = 0; x < current.heightMap.GetLength(0); x++)
                {
                    for (int y = 0; y < current.heightMap.GetLength(1); y++)
                    {
                        linearity += current.heightMap[x, y];
                    }
                }

                linearity = linearity/(current.heightMap.GetLength(0)*current.heightMap.GetLength(1));

                Debug.Log("Test "+i+ " Linearity:"+ linearity);
                linearityArray[i] = linearity;
            }

            double[] oceanArray = new double[Tests];

            for (int i = 0; i < Tests; i++)
            {

                

                MapData current = ExpressiveMapDatas[i];

                double ocean = 0.0f;

                double lowestLand = 1.0;

                for (int x = 0; x < current.heightMap.GetLength(0); x++)
                {
                    for (int y = 0; y < current.heightMap.GetLength(1); y++)
                    {
                        if (current.heightMap[x, y] <= lowestLand)
                        {
                            lowestLand = current.heightMap[x, y];
                        }
                    }
                }

                for (int x = 0; x < current.heightMap.GetLength(0); x++)
                {
                    for (int y = 0; y < current.heightMap.GetLength(1); y++)
                    {
                        if (current.heightMap[x, y] <= lowestLand + 0.08f)
                        {
                            ocean += 1;
                        }
                    }
                }

                ocean = ocean / (current.heightMap.GetLength(0) * current.heightMap.GetLength(1));

                Debug.Log("Test " + i + " Oceanity:" + ocean);
                oceanArray[i] = ocean;
            }

            const int textureSize = 1000;

            Texture2D texture = new Texture2D(textureSize, textureSize);

            for (int y = 0; y < textureSize; y++)
            {
                for (int x = 0; x < textureSize; x++)
                {
                    texture.SetPixel(x, y, Color.black);
                }
            }
            //Linearity X
            //0.08-0.12
            double minLinearity = 0;
            double maxLinearity = 1.00;
            
            //Oceanity Y
            //0.40-0.70
            double minOceanity = 0;
            double maxOceanity = 1.00;



            for (int i = 0; i < Tests; i++)
            {

                double valueLinearity = linearityArray[i];
                double percentageLinMin = valueLinearity - minLinearity > 0 ? valueLinearity - minLinearity : 0;
                double percentageLinMax = maxLinearity - minLinearity;

                double percentageLinearity = percentageLinMin/percentageLinMax;

                double valueOceanity = oceanArray[i];
                double percentageOceMin = valueOceanity - minOceanity > 0 ? valueOceanity - minOceanity : 0;
                double percentageOceMax = maxOceanity - minOceanity;

                double percentageOceanity = percentageOceMin / percentageOceMax;

                int percLinear = (int) (percentageLinearity * textureSize);
                int percOcean = (int)(percentageOceanity * textureSize);

                Color oldColor = texture.GetPixel(percLinear, percOcean);
                if (oldColor.r < 1.0f)
                {
                    //not maxed
                    oldColor.r += 0.25f;
                    oldColor.g += 0.25f;
                    oldColor.b += 0.25f;
                }
                texture.SetPixel(percLinear, percOcean, oldColor);

            }


            if (!string.IsNullOrEmpty(path))
            {
                UnityEditor.AssetDatabase.CreateAsset(texture, path);
                
                byte[] bytes = texture.EncodeToPNG();
                File.WriteAllBytes(path + ".Picture.png", bytes);

                path = null;
            }
        }
    }

}