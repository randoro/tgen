﻿using UnityEngine;
using System.Collections;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;

namespace NodeEditorFramework.Standard
{
	[System.Serializable]
	[Node (false, "Input/Float")]
	public class InputFloatNode : Node 
	{
		public const string ID = "inputFloatNode";
		public override string GetID { get { return ID; } }

        [SerializeField]
        public float Value;

		public override Node Create (Vector2 pos) 
		{
            InputFloatNode node = CreateInstance <InputFloatNode> ();

			node.name = "Float";
			node.rect = new Rect (pos.x, pos.y, 100, 50);;

			NodeOutput.Create (node, "Value", "Float");

			return node;
		}

		protected internal override void NodeGUI () 
		{
			Value = RTEditorGUI.FloatField (new GUIContent ("Value", "The input value of type float"), Value);
            OutputKnob (0);

			if (GUI.changed)
				NodeEditor.RecalculateFrom (this);
		}

		public override bool Calculate () 
		{
			Outputs[0].SetValue<float> (Value);
			return true;
		}
	}
}