﻿using UnityEngine;
using System.Collections;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;

namespace NodeEditorFramework.Standard
{
	[System.Serializable]
	[Node (false, "Input/Integer")]
	public class InputIntNode : Node 
	{
		public const string ID = "inputIntNode";
		public override string GetID { get { return ID; } }

        [SerializeField]
		public int Value;

		public override Node Create (Vector2 pos) 
		{
			InputIntNode node = CreateInstance <InputIntNode> ();

			node.name = "Integer";
			node.rect = new Rect (pos.x, pos.y, 100, 50);;

			NodeOutput.Create (node, "Value", "Int");

			return node;
		}

		protected internal override void NodeGUI () 
		{
			Value = RTEditorGUI.IntField (new GUIContent ("Value", "The input value of type int"), Value);
            OutputKnob (0);

			if (GUI.changed)
				NodeEditor.RecalculateFrom (this);
		}

		public override bool Calculate () 
		{
			Outputs[0].SetValue<int> (Value);
			return true;
		}
	}
}