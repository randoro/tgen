using System;
using UnityEngine;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using UnityEditor;
using WorleyNoise;

namespace NodeEditorFramework.Standard
{
	[Node (false, "Noise/Worley Noise")]
	public class WorleyNode : Node 
	{
		public const string ID = "worleyNode";
		public override string GetID { get { return ID; } }
        
        [SerializeField]
        public int Scale;
        [SerializeField]
        public int PointsPerSquare;
        [SerializeField]
        public DistanceFunction DistanceFunction;
        [SerializeField]
        public ValueFunction ValueFunction;
        [SerializeField]
        public int Seed;

        public override Node Create (Vector2 pos) 
		{
            WorleyNode node = CreateInstance<WorleyNode> ();
			
			node.rect = new Rect (pos.x, pos.y, 280, 180);
			node.name = "Worley Noise";

            Scale = 64;
            PointsPerSquare = 3;
            DistanceFunction = DistanceFunction.Euclidean;
            ValueFunction = ValueFunction.SecondTakeFirst;
            Seed = 0;

            node.CreateInput ("Seed", "Int");
            node.CreateOutput ("Output", "WorleySet");

            return node;
		}
		
		protected internal override void NodeGUI () 
		{
			GUILayout.BeginHorizontal ();
			GUILayout.BeginVertical ();

			//Inputs [0].DisplayLayout ();

			GUILayout.EndVertical ();
            GUILayout.BeginVertical ();
			
			Outputs [0].DisplayLayout ();

            GUILayout.EndVertical ();
            GUILayout.EndHorizontal ();
            
            GUILayout.Label("Worley Noise Settings");
            RTEditorGUI.Seperator();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Scale", "Defines the scale of the Perlin noise."), GUILayout.Width(100));
		    Scale = RTEditorGUI.IntField(Scale, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("PointsPerSquare", "Defines the number of points used in each square."), GUILayout.Width(100));
            PointsPerSquare = RTEditorGUI.IntSlider(PointsPerSquare, 1, 5, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
#if UNITY_EDITOR
            DistanceFunction = (DistanceFunction)RTEditorGUI.EnumPopup(new GUIContent("Distance Function", "The type of distance function used in the calculation."), DistanceFunction);
#endif
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
#if UNITY_EDITOR
            ValueFunction = (ValueFunction)RTEditorGUI.EnumPopup(new GUIContent("Value Function", "The type of value function used in the calculation."), ValueFunction);
#endif
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Seed", "Defines the seed used."), GUILayout.Width(100));
		    if (Inputs[0].connection != null)
		    {
		        GUILayout.Label(Seed.ToString());
		    }
		    else
		    {
		        Seed = RTEditorGUI.IntField(Seed, GUILayout.Width(150));
            }
		    InputKnob(0);
            GUILayout.EndHorizontal();

            if (GUI.changed)
                NodeEditor.RecalculateFrom(this);
        }
		
		public override bool Calculate () 
		{
            //if (!allInputsReady ()) //maybe need change?
            //	return false;
            if (Inputs[0].connection != null)
            {
                Seed = Inputs[0].GetValue<int>();
            }
		    if (Scale == 0)
		    {
		        Scale = 1;
		    }
		    if (PointsPerSquare == 0)
		    {
		        PointsPerSquare = 1;
		    }

		    Outputs[0].SetValue<WorleySet>(new WorleySet(Scale, PointsPerSquare, DistanceFunction, ValueFunction, Seed));
            
            return true;
		}
	}

}