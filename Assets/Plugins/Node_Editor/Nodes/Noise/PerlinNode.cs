using System;
using UnityEngine;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using UnityEditor;

namespace NodeEditorFramework.Standard
{
	[Node (false, "Noise/Perlin Noise")]
	public class PerlinNode : Node 
	{
		public const string ID = "perlinNode";
		public override string GetID { get { return ID; } }

        [SerializeField]
        public int Scale;
        [SerializeField]
        public int Octaves;
        [SerializeField]
        public float Persistance;
        [SerializeField]
        public float Lacunarity;
        [SerializeField]
        public int Seed;
        [SerializeField]
        public int OffsetX;
        [SerializeField]
        public int OffsetY;

        public override Node Create (Vector2 pos) 
		{
            PerlinNode node = CreateInstance<PerlinNode> ();
			
			node.rect = new Rect (pos.x, pos.y, 280, 180);
			node.name = "Perlin Noise";

            Scale = 1;
            Octaves = 0;
            Persistance = 0.5f;
            Lacunarity = 1;
            Seed = 0;
            OffsetX = 0;
            OffsetY = 0;

            node.CreateInput ("Seed", "Int");
            node.CreateOutput ("Output", "PerlinSet");

            return node;
		}
		
		protected internal override void NodeGUI () 
		{
			GUILayout.BeginHorizontal ();
			GUILayout.BeginVertical ();

			//Inputs [0].DisplayLayout ();

			GUILayout.EndVertical ();
            GUILayout.BeginVertical ();
			
			Outputs [0].DisplayLayout ();

            GUILayout.EndVertical ();
            GUILayout.EndHorizontal ();
            
            GUILayout.Label("Perlin Noise Settings");
            RTEditorGUI.Seperator();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Scale", "Defines the scale of the Perlin noise."), GUILayout.Width(100));
		    Scale = RTEditorGUI.IntField(Scale, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Octaves", "Defines the number of octaves used."), GUILayout.Width(100));
            Octaves = RTEditorGUI.IntSlider(Octaves, 0, 12, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Persistence", "Defines to which degree the amplitude diminish for each octave."), GUILayout.Width(100));
            Persistance = RTEditorGUI.Slider(Persistance, 0, 1f, GUILayout.Width(150));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Lacunarity", "Defines to which degree the frequency increases for each octave."), GUILayout.Width(100));
            Lacunarity = RTEditorGUI.Slider(Lacunarity, 1f, 3f, GUILayout.Width(150));
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Seed", "Defines the seed used."), GUILayout.Width(100));
		    if (Inputs[0].connection != null)
		    {
		        GUILayout.Label(Seed.ToString());
		    }
		    else
		    {
		        Seed = RTEditorGUI.IntField(Seed, GUILayout.Width(150));
            }
		    InputKnob(0);
            GUILayout.EndHorizontal();

            if (GUI.changed)
                NodeEditor.RecalculateFrom(this);
        }
		
		public override bool Calculate () 
		{
            //if (!allInputsReady ()) //maybe need change?
            //	return false;
            if (Inputs[0].connection != null)
            {
                Seed = Inputs[0].GetValue<int>();
            }
            if (Scale == 0)
            {
                Scale = 1;
            }
		    if (Octaves == 0)
		    {
		        Octaves = 1;
		    }
		    if (Lacunarity <= 0.0f)
		    {
		        Lacunarity = 1;
		    }

            Outputs[0].SetValue<PerlinSet> (new PerlinSet(Scale, Octaves, Persistance, Lacunarity, Seed, OffsetX, OffsetY));
            
            return true;
		}
	}

}