using System;
using UnityEngine;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using UnityEditor;
using WorleyNoise;

namespace NodeEditorFramework.Standard
{
	[Node (false, "Manipulation/Merge Noise")]
	public class MergeNode : Node 
	{
		public const string ID = "mergeNode";
		public override string GetID { get { return ID; } }
        
        [SerializeField]
        public Set Set1;
        [SerializeField]
        public Set Set2;
        [SerializeField]
        public float QuantitySet1;
        [SerializeField]
        public float QuantitySet2;
        [SerializeField]
        public MergeFunction MergeFunction;

        public override Node Create (Vector2 pos) 
		{
            MergeNode node = CreateInstance<MergeNode> ();
			
			node.rect = new Rect (pos.x, pos.y, 280, 180);
			node.name = "Merge Noise";

            QuantitySet1 = 0.5f;
            QuantitySet2 = 0.5f;
            MergeFunction = MergeFunction.Add;

            node.CreateInput ("Noise 1", "Set");
            node.CreateInput ("Noise 2", "Set");
            node.CreateOutput ("Output", "MergeSet");

            return node;
		}
		
		protected internal override void NodeGUI () 
		{
			GUILayout.BeginHorizontal ();
			GUILayout.BeginVertical ();

			//Inputs [0].DisplayLayout ();

			GUILayout.EndVertical ();
            GUILayout.BeginVertical ();
			
			Outputs [0].DisplayLayout ();

            GUILayout.EndVertical ();
            GUILayout.EndHorizontal ();
            
            GUILayout.Label("Merge Noise Settings");
            RTEditorGUI.Seperator();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Quantity Noise 1", "Defines to which degree the first noise is merged into the output."), GUILayout.Width(100));
            QuantitySet1 = RTEditorGUI.Slider(QuantitySet1, 0f, 1f, GUILayout.Width(150));
            InputKnob(0);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label(new GUIContent("Quantity Noise 2", "Defines to which degree the second noise is merged into the output."), GUILayout.Width(100));
            QuantitySet2 = RTEditorGUI.Slider(QuantitySet2, 0f, 1f, GUILayout.Width(150));
            InputKnob(1);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
#if UNITY_EDITOR
            MergeFunction = (MergeFunction)RTEditorGUI.EnumPopup(new GUIContent("Merge Function", "The type of merge function used in the calculation."),MergeFunction);
            //MergeFunction = (MergeFunction)UnityEditor.EditorGUILayout.EnumPopup(new GUIContent("Merge Function", "The type of merge function used in the calculation."), MergeFunction);
#endif
            GUILayout.EndHorizontal();
            

            if (GUI.changed)
                NodeEditor.RecalculateFrom(this);
        }
		
		public override bool Calculate () 
		{
            //if (!allInputsReady ()) //maybe need change?
            //	return false;
            if (Inputs[0].connection != null)
            {
                Set1 = Inputs[0].GetValue<Set>();
            }
            if (Inputs[1].connection != null)
            {
                Set2 = Inputs[1].GetValue<Set>();
            }


            Outputs[0].SetValue<MergeSet>(new MergeSet(Set1, Set2, QuantitySet1, QuantitySet2, MergeFunction));
            
            return true;
		}
	}

}