﻿using UnityEngine;
using System.Collections;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using TGen.Standard;

namespace NodeEditorFramework.Standard
{
	[System.Serializable]
	[Node (false, "Display/Display Texture")]
	public class DisplayTextureNode : Node 
	{
		public const string ID = "displayTextureNode";
		public override string GetID { get { return ID; } }

		
		private bool _assigned = false;
		private Set _value;
	    private Texture2D _perlin;

        public override Node Create (Vector2 pos) 
		{
            DisplayTextureNode node = CreateInstance <DisplayTextureNode> ();
			
			node.name = "Display Texture";
			node.rect = new Rect (pos.x, pos.y, 200, 220);
			
			NodeInput.Create (node, "Value", "Set");
            return node;
		}
		
		protected internal override void NodeGUI () 
		{
		    if (_assigned && _perlin != null)
		    {
		        //GUILayout.Box(_perlin); //, GUILayout.MaxWidth(188), GUILayout.MaxHeight(188));
                //GUILayout.Box(_perlin)
                    GUI.DrawTexture(new Rect(2, 2, 195, 195), _perlin);
                //GUILayoutUtility.GetRect();
		        //GUI.DrawTexture(Rect(Screen.width * 0.1, Screen.height * 0.1, 100, 100), fuelBarTex);
		    }
		}
		
		public override bool Calculate () 
		{
			if (!allInputsReady ()) 
			{
                //_value = 0;
                _assigned = false;
				return false;
			}

            _value = Inputs[0].connection.GetValue<Set>();
            _assigned = true;

            if(_value.GetType() == typeof(PerlinSet))
		    {
                _perlin = TextureGenerator.TextureFromPerlinSet(256, 256, (PerlinSet)_value);
            }
            else if (_value.GetType() == typeof (WorleySet))
            {
                _perlin = TextureGenerator.TextureFromWorleySet(256, 256, (WorleySet) _value);
            }
		    return true;
		}
	}
}