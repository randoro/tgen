﻿using UnityEngine;
using System.Collections;
using NodeEditorFramework;

namespace NodeEditorFramework.Standard
{
	[System.Serializable]
	[Node (false, "Display/Display Int")]
	public class DisplayIntNode : Node 
	{
		public const string ID = "displayIntNode";
		public override string GetID { get { return ID; } }

		
		private bool _assigned = false;
		private int _value;

		public override Node Create (Vector2 pos) 
		{
            DisplayIntNode node = CreateInstance <DisplayIntNode> ();
			
			node.name = "Display Int";
			node.rect = new Rect (pos.x, pos.y, 100, 50);
			
			NodeInput.Create (node, "Value", "Int");
            return node;
		}
		
		protected internal override void NodeGUI () 
		{
			Inputs [0].DisplayLayout (new GUIContent ("Value : " + (_assigned ? _value.ToString () : ""), "The input value to display"));
		}
		
		public override bool Calculate () 
		{
			if (!allInputsReady ()) 
			{
                _value = 0;
                _assigned = false;
				return false;
			}

            _value = Inputs[0].connection.GetValue<int>();
            _assigned = true;

			return true;
		}
	}
}