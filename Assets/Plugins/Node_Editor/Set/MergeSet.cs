﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorleyNoise;

namespace NodeEditorFramework.Standard
{

    public enum MergeFunction { Add, Multiply } //, Average, Subtract, Multiply, Mask }

    public class MergeSet : Set
    {
        public Set Set1 { get; protected set; }
        public Set Set2 { get; protected set; }
        public float QuantitySet1 { get; protected set; }
        public float QuantitySet2 { get; protected set; }
        public MergeFunction MergeFunction { get; protected set; }

        public MergeSet(Set set1, Set set2, float quantitySet1, float quantitySet2, MergeFunction mergeFunction)
        {
            this.Set1 = set1;
            this.Set2 = set2;
            this.QuantitySet1 = quantitySet1;
            this.QuantitySet2 = quantitySet2;
            this.MergeFunction = mergeFunction;
        }
    }

    public class FoldSetType : IConnectionTypeDeclaration
    {
        public string Identifier { get { return "MergeSet"; } }
        public Type Type { get { return typeof(MergeSet); } }
        public Color Color { get { return Color.yellow; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }
}
