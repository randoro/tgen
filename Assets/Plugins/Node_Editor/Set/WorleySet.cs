﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorleyNoise;

namespace NodeEditorFramework.Standard
{
    public class WorleySet : Set
    {
        public int Scale { get; protected set; }
        public int PointsPerSquare { get; protected set; }
        public DistanceFunction DistanceFunction { get; protected set; }
        public ValueFunction ValueFunction { get; protected set; }
        public int Seed { get; protected set; }

        public WorleySet(int scale, int pointsPerSquare, DistanceFunction distanceFunction, ValueFunction valueFunction, int seed)
        {
            this.Scale = scale;
            this.PointsPerSquare = pointsPerSquare;
            this.DistanceFunction = distanceFunction;
            this.ValueFunction = valueFunction;
            this.Seed = seed;
        }
    }

    public class WorleySetType : IConnectionTypeDeclaration
    {
        public string Identifier { get { return "WorleySet"; } }
        public Type Type { get { return typeof(WorleySet); } }
        public Color Color { get { return Color.yellow; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }
}
