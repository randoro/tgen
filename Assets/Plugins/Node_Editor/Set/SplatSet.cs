using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NodeEditorFramework.Standard
{
    public class SplatSet : Set
    {
        public float[,] heights { get; set; }
        public float[] curveValue1;
        public float[] curveValue2;
        public float[] curveValue3;
        public float[] curveValue4;
        public Texture2D T1 { get; protected set; }
        public Texture2D T1N { get; protected set; }
        public AnimationCurve T1C { get; protected set; }
        public Texture2D T2 { get; protected set; }
        public Texture2D T2N { get; protected set; }
        public AnimationCurve T2C { get; protected set; }
        public Texture2D T3 { get; protected set; }
        public Texture2D T3N { get; protected set; }
        public AnimationCurve T3C { get; protected set; }
        public Texture2D T4 { get; protected set; }
        public Texture2D T4N { get; protected set; }
        public AnimationCurve T4C { get; protected set; }

        public SplatSet(Texture2D T1, Texture2D T1N, AnimationCurve T1C, Texture2D T2, Texture2D T2N, AnimationCurve T2C, Texture2D T3, Texture2D T3N, AnimationCurve T3C, Texture2D T4, Texture2D T4N, AnimationCurve T4C)
        {
            heights = new float[0,0];
            curveValue1 = new float[100];
            curveValue2 = new float[100];
            curveValue3 = new float[100];
            curveValue4 = new float[100];
            this.T1 = T1;
            this.T1N = T1N;
            this.T1C = T1C;
            this.T2 = T2;
            this.T2N = T2N;
            this.T2C = T2C;
            this.T3 = T3;
            this.T3N = T3N;
            this.T3C = T3C;
            this.T4 = T4;
            this.T4N = T4N;
            this.T4C = T4C;
        }

    }

    public class SplatSetType : IConnectionTypeDeclaration
    {
        public string Identifier { get { return "SplatSet"; } }
        public Type Type { get { return typeof(SplatSet); } }
        public Color Color { get { return Color.red; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }
}
