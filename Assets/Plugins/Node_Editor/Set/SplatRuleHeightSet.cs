using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NodeEditorFramework.Standard
{
    public class SplatRuleHeightSet : Set
    {
        public AnimationCurve C { get; protected set; }

        public SplatRuleHeightSet(AnimationCurve C)
        {
            this.C = C;
        }

    }

    public class SplatRuleHeightSetType : IConnectionTypeDeclaration
    {
        public string Identifier { get { return "SplatRuleHeightSet"; } }
        public Type Type { get { return typeof(SplatRuleHeightSet); } }
        public Color Color { get { return Color.cyan; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }
}
