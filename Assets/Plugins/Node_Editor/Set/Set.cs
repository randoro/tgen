﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NodeEditorFramework.Standard
{
    public abstract class Set
    {
        //Just a holder for now.
    }

    public class SetType : IConnectionTypeDeclaration
    {
        public string Identifier { get { return "Set"; } }
        public Type Type { get { return typeof(Set); } }
        public Color Color { get { return Color.yellow; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }

}
