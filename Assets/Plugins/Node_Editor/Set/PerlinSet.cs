﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NodeEditorFramework.Standard
{
    public class PerlinSet : Set
    {
        public int Scale { get; protected set; }
        public int Octaves { get; protected set; }
        public float Persistance { get; protected set; }
        public float Lacunarity { get; protected set; }
        public int Seed { get; protected set; }
        public int OffsetX { get; protected set; }
        public int OffsetY { get; protected set; }

        public PerlinSet(int scale, int octaves, float persistance, float lacunarity, int seed, int offsetX, int offsetY)
        {
            this.Scale = scale;
            this.Octaves = octaves;
            this.Persistance = persistance;
            this.Lacunarity = lacunarity;
            this.Seed = seed;
            this.OffsetX = offsetX;
            this.OffsetY = offsetY;
        }
    }

    public class PerlinSetType : IConnectionTypeDeclaration
    {
        public string Identifier { get { return "PerlinSet"; } }
        public Type Type { get { return typeof(PerlinSet); } }
        public Color Color { get { return Color.yellow; } }
        public string InKnobTex { get { return "Textures/In_Knob.png"; } }
        public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
    }
}
